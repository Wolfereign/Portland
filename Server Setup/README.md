# Setup Scripts To Configure The Server
Non-Sensitive information is statically set in the script files and is specific to my server.  This includes the configuration for ZFS, Samba, msmtp, and Users.  Sensitive information such as passwords are prompted for.

All Scripts are based on Ubuntu Server 16 LTS.

## Options/Roles, selected during installation, should be: 
    UEFI  
    Standard System Utilities  
    OpenSSH Server  

## Configuration
    Uncomplicated Firewall (UFW)
    SSH/SFTP for remote management
    ZFS for Software Raid
    msmtp for email notifications
    Docker for Running Services (Plex, Deluge, etc..)
    KVM/Qemu for windows virtual machines
    Cockpit for web gui managment of server
    Samba for direct media library access from windows (will containerize eventually)
