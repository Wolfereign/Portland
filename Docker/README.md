# Scripts For Configuring Docker Containers
Settings such as Volumes and UID/GID are specific to my server setup.  Sensitive information such as passwords are prompted for.

## Working Services
    Plex Media Server
    Deluge + OpenVPN (PIA)
    Dynamic DNS Updater (Google Domains)
    Dynamic DNS Updater (Duck DNS)
    FreeIPA (Including DNS)
    Samba
    Eclipse Che
    Ark Survival Evolved Server
    Watchtower (Auto Update Containers)

## Not Yet Working Services
    OpenVPN Server
